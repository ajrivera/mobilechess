package com.example.myapplication;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.LinkedList;
import java.util.List;

public class Pawn extends Piece {

    private boolean wasMoved;

    public Pawn(int color, Casilla initSq, int sprite) {
        super(color, initSq, sprite);
        wasMoved = false;
    }

   /* public Pawn(PieceType type, int color, int sprite) {
        super(type, color, sprite);
        wasMoved = false;
    }*/


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean move(Casilla fin) {
        boolean b = super.move(fin);
        wasMoved = true;
        return b;
    }

    @Override
    public List<Casilla> getLegalMoves(InGameActivity b) {
        LinkedList<Casilla> legalMoves = new LinkedList<Casilla>();

        Casilla[][] board = b.board;

        Log.e("pieza", String.valueOf(this.getPosition().getXNum()));
        Log.e("pieza", String.valueOf(this.getPosition().getYNum()));

        int x = this.getPosition().getXNum();
        int y = this.getPosition().getYNum();
        int c = this.getColor();

        if (c == 0) {
            if (!wasMoved) {
                if (!board[y+2][x].isOccupied()) {
                    legalMoves.add(board[y+2][x]);
                }
            }

            if (y+1 < 8) {
                if (!board[y+1][x].isOccupied()) {
                    legalMoves.add(board[y+1][x]);
                }
            }

            if (x+1 < 8 && y+1 < 8) {
                if (board[y+1][x+1].isOccupied()) {
                    legalMoves.add(board[y+1][x+1]);
                }
            }

            if (x-1 >= 0 && y+1 < 8) {
                if (board[y+1][x-1].isOccupied()) {
                    legalMoves.add(board[y+1][x-1]);
                }
            }
        }

        if (c == 1) {
            if (!wasMoved) {
                if (!board[y-2][x].isOccupied()) {
                    legalMoves.add(board[y-2][x]);
                }
            }

            if (y-1 >= 0) {
                if (!board[y-1][x].isOccupied()) {
                    legalMoves.add(board[y-1][x]);
                }
            }

            if (x+1 < 8 && y-1 >= 0) {
                if (board[y-1][x+1].isOccupied()) {
                    legalMoves.add(board[y-1][x+1]);
                }
            }

            if (x-1 >= 0 && y-1 >= 0) {
                if (board[y-1][x-1].isOccupied()) {
                    legalMoves.add(board[y-1][x-1]);
                }
            }
        }

        return legalMoves;
    }

    /*
    @Override
    public boolean canMove(Casilla inicio, Casilla destino) {
        if(inicio.pos.x - destino.pos.x == 1) {
            return true;
        }
        else{
            return false;
        }
    }*/


}
