package com.example.myapplication;

import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.LinkedList;
import java.util.List;

public abstract class Piece {

    PieceType type;
    int color; //1->BLACK 0->WHITE
    int sprite;
  //  boolean active;
    Casilla currentSquare;

   /* public Piece(PieceType type, int color, int sprite) {
        this.type = type;
        this.color = color;
        this.sprite = sprite;
      //  active = true;
    }*/


    public Piece(int color, Casilla initSq, int sprite) {
        this.color = color;
        this.currentSquare = initSq;
        this.sprite = sprite;
    }

    public Casilla getPosition() {
        return currentSquare;
    }
    public void setPosition(Casilla sq) {
        this.currentSquare = sq;
    }

    public int getColor() {
        return color;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean move(Casilla fin) {
        Piece occup = fin.getOccupyingPiece();

        if (occup != null) {
            if (occup.getColor() == this.color) return false;
            else fin.capture(this);
        }

        currentSquare.removePiece();
        this.currentSquare = fin;
        currentSquare.put(this);
        return true;
    }

    public void draw() {
        Drawable drawable = currentSquare.instanceBoard.getResources().getDrawable(sprite);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                currentSquare.button.setForeground(drawable);
        }
    }


 /*   public boolean canMove(Casilla inicio, Casilla destino){
        return true;
    }*/

    @Override
    public String toString() {
        return "Piece{" +
              //  "type=" + type +
                ", color=" + color +
                ", sprite=" + sprite +
            //    ", active=" + active +
                '}';
    }

    public int[] getLinearOccupations(Casilla[][] board, int x, int y) {
        int lastYabove = 0;
        int lastXright = 7;
        int lastYbelow = 7;
        int lastXleft = 0;

        for (int i = 0; i < y; i++) {
            if (board[i][x].isOccupied()) {
                if (board[i][x].getOccupyingPiece().getColor() != this.color) {
                    lastYabove = i;
                } else lastYabove = i + 1;
            }
        }

        for (int i = 7; i > y; i--) {
            if (board[i][x].isOccupied()) {
                if (board[i][x].getOccupyingPiece().getColor() != this.color) {
                    lastYbelow = i;
                } else lastYbelow = i - 1;
            }
        }

        for (int i = 0; i < x; i++) {
            if (board[y][i].isOccupied()) {
                if (board[y][i].getOccupyingPiece().getColor() != this.color) {
                    lastXleft = i;
                } else lastXleft = i + 1;
            }
        }

        for (int i = 7; i > x; i--) {
            if (board[y][i].isOccupied()) {
                if (board[y][i].getOccupyingPiece().getColor() != this.color) {
                    lastXright = i;
                } else lastXright = i - 1;
            }
        }

        int[] occups = {lastYabove, lastYbelow, lastXleft, lastXright};

        return occups;
    }

    public List<Casilla> getDiagonalOccupations(Casilla[][] board, int x, int y) {
        LinkedList<Casilla> diagOccup = new LinkedList<Casilla>();

        int xNW = x - 1;
        int xSW = x - 1;
        int xNE = x + 1;
        int xSE = x + 1;
        int yNW = y - 1;
        int ySW = y + 1;
        int yNE = y - 1;
        int ySE = y + 1;

        while (xNW >= 0 && yNW >= 0) {
            if (board[yNW][xNW].isOccupied()) {
                if (board[yNW][xNW].getOccupyingPiece().getColor() == this.color) {
                    break;
                } else {
                    diagOccup.add(board[yNW][xNW]);
                    break;
                }
            } else {
                diagOccup.add(board[yNW][xNW]);
                yNW--;
                xNW--;
            }
        }

        while (xSW >= 0 && ySW < 8) {
            if (board[ySW][xSW].isOccupied()) {
                if (board[ySW][xSW].getOccupyingPiece().getColor() == this.color) {
                    break;
                } else {
                    diagOccup.add(board[ySW][xSW]);
                    break;
                }
            } else {
                diagOccup.add(board[ySW][xSW]);
                ySW++;
                xSW--;
            }
        }

        while (xSE < 8 && ySE < 8) {
            if (board[ySE][xSE].isOccupied()) {
                if (board[ySE][xSE].getOccupyingPiece().getColor() == this.color) {
                    break;
                } else {
                    diagOccup.add(board[ySE][xSE]);
                    break;
                }
            } else {
                diagOccup.add(board[ySE][xSE]);
                ySE++;
                xSE++;
            }
        }

        while (xNE < 8 && yNE >= 0) {
            if (board[yNE][xNE].isOccupied()) {
                if (board[yNE][xNE].getOccupyingPiece().getColor() == this.color) {
                    break;
                } else {
                    diagOccup.add(board[yNE][xNE]);
                    break;
                }
            } else {
                diagOccup.add(board[yNE][xNE]);
                yNE--;
                xNE++;
            }
        }

        return diagOccup;
    }

    public abstract List<Casilla> getLegalMoves(InGameActivity b);
}
