package com.example.myapplication;

import java.util.List;

public class Bishop extends Piece {
    public Bishop(int color, Casilla initSq, int sprite) {
        super(color, initSq, sprite);
    }

   /* public Bishop(PieceType type, int color, int sprite) {
        super(type, color, sprite);
    }*/

    @Override
    public List<Casilla> getLegalMoves(InGameActivity b) {
        Casilla[][] board = b.board;
        int x = this.getPosition().getXNum();
        int y = this.getPosition().getYNum();

        return getDiagonalOccupations(board, x, y);
    }
}
