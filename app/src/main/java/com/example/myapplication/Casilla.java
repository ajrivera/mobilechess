package com.example.myapplication;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class Casilla extends AppCompatActivity{
    public Button button;
    InGameActivity instanceBoard;
    Piece actualPiece;
    Vector2 pos;

    int color;
    boolean dispPiece;

    public Casilla(InGameActivity instanceBoard, int c, int x, int y){
        this.instanceBoard = instanceBoard;
        pos = new Vector2();
        pos.x = x;
        pos.y = y;
        color = c;
        dispPiece = true;
    }

    public Piece getOccupyingPiece() {
        return actualPiece;
    }

    public boolean isOccupied() {
        return (this.actualPiece != null);
    }

    public int getXNum() {
        return pos.y;
    }

    public int getYNum() {
        return pos.x;
    }

    public void setDisplay(boolean v) {
        this.dispPiece = v;
    }

    public void setCasilla(int casilla) {
        button = (Button) instanceBoard.findViewById(casilla);
        button.setOnClickListener(instanceBoard);
    }

    public void capture(Piece p) {
        Piece k = getOccupyingPiece();
        if (k.getColor() == 0) instanceBoard.Bpieces.remove(k);
        if (k.getColor() == 1) instanceBoard.Wpieces.remove(k);
        this.actualPiece = p;
    }

 /*   @RequiresApi(api = Build.VERSION_CODES.M)
    public void setPiece(Piece piece) {
        actualPiece = piece;
        Drawable drawable = instanceBoard.getResources().getDrawable(piece.sprite);
        button.setForeground(drawable);
       // button.setForeground(piece.sprite);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    public Piece removePiece() {
        Piece p = actualPiece;
        actualPiece = null;
       // button.setForeground(null);
        return p;
    }

    public void put(Piece p) {
        this.actualPiece = p;
        p.setPosition(this);
    }

    public void paintComponent() {
        if(actualPiece != null && dispPiece) {
            actualPiece.draw();
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            button.setForeground(null);
        }
    }



}
