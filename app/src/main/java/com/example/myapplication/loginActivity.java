package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class loginActivity extends AppCompatActivity {
    ArrayAdapter adaptador;
    public static HttpURLConnection con;

    //public static String rutaServer = "https://virtual395.ies-sabadell.cat/virtual395/chess/send_data.php";
    public static String rutaServer = "http://192.168.1.109/chess/get_data.php";
    public static String user = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        this.setTitle("ChessGame");
        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        Button loginButton = (Button) findViewById(R.id.ButtonLogin);


        loginButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bajadaBBDD();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    void bajadaBBDD() throws IOException {
        Intent inGameIntent = new Intent(this, InGameActivity.class);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, rutaServer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject responseJson = new JSONObject(response);
                            id idModel = new Gson().fromJson(responseJson.getString("id"), id.class);
                            startActivity(inGameIntent);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "User Not Found", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                EditText username = (EditText) findViewById(R.id.userLogin);
                EditText password = (EditText) findViewById(R.id.passwordLogin);
                params.put("username", username.getText().toString());
                params.put("password", password.getText().toString());
                Log.e("WTFF", username.getText().toString());
                Log.e("WTFFF", password.getText().toString());
                user = username.getText().toString();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

// To prevent timeout error
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

// Add the request to the RequestQueue.
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);






        /*JSONObject postData = new JSONObject();
        try {
            EditText username = (EditText) findViewById(R.id.userLogin);
            EditText password = (EditText) findViewById(R.id.passwordLogin);
            postData.put("username", username);
            postData.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, rutaServer, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("WTF", response.toString() );
                System.out.println(response);
                Toast.makeText(loginActivity.this,response.toString(),Toast.LENGTH_LONG);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(jsonObjectRequest);
    */
    }
    }
