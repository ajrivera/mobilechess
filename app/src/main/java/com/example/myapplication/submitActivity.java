package com.example.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;


public class submitActivity extends AppCompatActivity {
    ArrayAdapter adaptador;
    public static HttpURLConnection con;
    String id = "prueba";
    int points = 0;

    //public static String rutaServer = "https://virtual395.ies-sabadell.cat/virtual395/chess/send_data.php";
    public static String rutaServer = "http://192.168.1.109/chess/send_data.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_submit);
        this.setTitle("ChessGame");
        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        Button submitButton = (Button) findViewById(R.id.buttonSubmit);

        submitButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subidBBDD();
            }
        });
    }

    void subidBBDD(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, rutaServer, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplication(), response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(submitActivity.this, error + "", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                EditText username = (EditText) findViewById(R.id.usernameSubmit);
                EditText password = (EditText) findViewById(R.id.PasswordSubmit);
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("username", String.valueOf(username.getText().toString()));
                params.put("password", String.valueOf(password.getText().toString()));
                params.put("points", String.valueOf(points));
                return params;
            }
        };

        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(stringRequest);
        Log.e("cache",cola.getCache().toString());
    }


    }
