package com.example.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.net.HttpURLConnection;


public class initActivity extends AppCompatActivity {
    ArrayAdapter adaptador;
    public static HttpURLConnection con;
    String id = "prueba";
    String username = "prueba";
    String password = "prueba";
    int points = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_init);
        this.setTitle("ChessGame");
        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);


        Button whitebutton = (Button) findViewById(R.id.White);
        Button blackbutton = (Button) findViewById(R.id.Black);

        Button submitButton = (Button) findViewById(R.id.initSubmitButton);
        Button loginButton = (Button) findViewById(R.id.buttonInitLogin);
        Button langButton = (Button) findViewById(R.id.changeLangButton);
        Intent submitIntent = new Intent(this, submitActivity.class);
        Intent loginIntent = new Intent(this, loginActivity.class);
        Intent langIntent = new Intent(Intent.ACTION_MAIN);
        Intent mainIntent = new Intent(this, InGameActivity.class);
        langIntent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if(prefs.getString("background","").contentEquals("white")){
            RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
            rl.setBackgroundColor(Color.WHITE);
        }else if(prefs.getString("background","").contentEquals("black")){
            RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
            rl.setBackgroundColor(Color.BLACK);
        }

        whitebutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
                rl.setBackgroundColor(Color.WHITE);
                prefs.edit().putString("background","white").commit();
            }
        });
        blackbutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
                rl.setBackgroundColor(Color.BLACK);
                prefs.edit().putString("background","black").commit();
            }
        });

        submitButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(submitIntent);
            }
        });

        loginButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!prefs.getString("username", "").contentEquals("")){
                    startActivity(mainIntent);
                }
                startActivity(loginIntent);
            }
        });

        langButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(langIntent);
            }
        });
    }
}
