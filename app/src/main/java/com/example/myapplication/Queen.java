package com.example.myapplication;

import java.util.LinkedList;
import java.util.List;

public class Queen extends Piece {
    public Queen(int color, Casilla initSq, int sprite) {
        super(color, initSq, sprite);
    }

    /*public Queen(PieceType type, int color, int sprite) {
        super(type, color, sprite);
    }*/

    @Override
    public List<Casilla> getLegalMoves(InGameActivity b) {
        LinkedList<Casilla> legalMoves = new LinkedList<Casilla>();
        Casilla[][] board = b.board;

        int x = this.getPosition().getXNum();
        int y = this.getPosition().getYNum();

        int[] occups = getLinearOccupations(board, x, y);

        for (int i = occups[0]; i <= occups[1]; i++) {
            if (i != y) legalMoves.add(board[i][x]);
        }

        for (int i = occups[2]; i <= occups[3]; i++) {
            if (i != x) legalMoves.add(board[y][i]);
        }

        List<Casilla> bMoves = getDiagonalOccupations(board, x, y);

        legalMoves.addAll(bMoves);

        return legalMoves;
    }


}
