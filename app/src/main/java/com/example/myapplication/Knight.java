package com.example.myapplication;

import java.util.LinkedList;
import java.util.List;

public class Knight extends Piece {
    public Knight(int color, Casilla initSq, int sprite) {
        super(color, initSq, sprite);
    }

   /* public Knight(PieceType type, int color, int sprite) {
        super(type, color, sprite);
    }*/

    @Override
    public List<Casilla> getLegalMoves(InGameActivity b) {
        LinkedList<Casilla> legalMoves = new LinkedList<Casilla>();
        Casilla[][] board = b.board;

        int x = this.getPosition().getXNum();
        int y = this.getPosition().getYNum();

        for (int i = 2; i > -3; i--) {
            for (int k = 2; k > -3; k--) {
                if(Math.abs(i) == 2 ^ Math.abs(k) == 2) {
                    if (k != 0 && i != 0) {
                        try {
                            legalMoves.add(board[y + k][x + i]);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            continue;
                        }
                    }
                }
            }
        }

        return legalMoves;
    }


}
