package com.example.myapplication;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InGameActivity extends AppCompatActivity implements View.OnClickListener{
    public Casilla[][] board;
    //ArrayList<Piece> whitePieces;
    //ArrayList<Piece> blackPieces;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        board = new Casilla[8][8];
        TextView name = (TextView) findViewById(R.id.player2name);
        MediaPlayer mp2 = MediaPlayer.create(InGameActivity.this, R.raw.ascensor);
        mp2.start();
        //preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if(prefs.getString("background","").contentEquals("white")){
            RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
            rl.setBackgroundColor(Color.WHITE);
        }else if(prefs.getString("background","").contentEquals("black")){
            RelativeLayout rl = (RelativeLayout)findViewById(R.id.layout);
            rl.setBackgroundColor(Color.BLACK);
        }

        Log.e("PREFS",prefs.getString("username", "xd"));
        if(!prefs.getString("username", "").contentEquals("")) {
            Log.e("ENTRA EN PREFS", "AAAAAAAAAAAAAAAAAAAAAAAAAAA");
            name.setText(prefs.getString("username", ""));
        }else{
            Log.e("NO HAY PREFS", "AAAAAAAAAAAAAAAAAAAAAAAAAAA");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("username", loginActivity.user).commit();
            name.setText(loginActivity.user);
        }

        whiteTurn = true;


        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                int xMod = i % 2;
                int yMod = j % 2;

                if ((xMod == 0 && yMod == 0) || (xMod == 1 && yMod == 1))
                    board[i][j] = new Casilla(this, 1, i, j);
                else
                    board[i][j] = new Casilla(this, 0, i, j);
            }
        }

        board[0][0].setCasilla(R.id.a8);
        board[1][0].setCasilla(R.id.a7);
        board[2][0].setCasilla(R.id.a6);
        board[3][0].setCasilla(R.id.a5);
        board[4][0].setCasilla(R.id.a4);
        board[5][0].setCasilla(R.id.a3);
        board[6][0].setCasilla(R.id.a2);
        board[7][0].setCasilla(R.id.a1);

        board[0][1].setCasilla(R.id.b8);
        board[1][1].setCasilla(R.id.b7);
        board[2][1].setCasilla(R.id.b6);
        board[3][1].setCasilla(R.id.b5);
        board[4][1].setCasilla(R.id.b4);
        board[5][1].setCasilla(R.id.b3);
        board[6][1].setCasilla(R.id.b2);
        board[7][1].setCasilla(R.id.b1);

        board[0][2].setCasilla(R.id.c8);
        board[1][2].setCasilla(R.id.c7);
        board[2][2].setCasilla(R.id.c6);
        board[3][2].setCasilla(R.id.c5);
        board[4][2].setCasilla(R.id.c4);
        board[5][2].setCasilla(R.id.c3);
        board[6][2].setCasilla(R.id.c2);
        board[7][2].setCasilla(R.id.c1);

        board[0][3].setCasilla(R.id.d8);
        board[1][3].setCasilla(R.id.d7);
        board[2][3].setCasilla(R.id.d6);
        board[3][3].setCasilla(R.id.d5);
        board[4][3].setCasilla(R.id.d4);
        board[5][3].setCasilla(R.id.d3);
        board[6][3].setCasilla(R.id.d2);
        board[7][3].setCasilla(R.id.d1);

        board[0][4].setCasilla(R.id.e8);
        board[1][4].setCasilla(R.id.e7);
        board[2][4].setCasilla(R.id.e6);
        board[3][4].setCasilla(R.id.e5);
        board[4][4].setCasilla(R.id.e4);
        board[5][4].setCasilla(R.id.e3);
        board[6][4].setCasilla(R.id.e2);
        board[7][4].setCasilla(R.id.e1);

        board[0][5].setCasilla(R.id.f8);
        board[1][5].setCasilla(R.id.f7);
        board[2][5].setCasilla(R.id.f6);
        board[3][5].setCasilla(R.id.f5);
        board[4][5].setCasilla(R.id.f4);
        board[5][5].setCasilla(R.id.f3);
        board[6][5].setCasilla(R.id.f2);
        board[7][5].setCasilla(R.id.f1);

        board[0][6].setCasilla(R.id.g8);
        board[1][6].setCasilla(R.id.g7);
        board[2][6].setCasilla(R.id.g6);
        board[3][6].setCasilla(R.id.g5);
        board[4][6].setCasilla(R.id.g4);
        board[5][6].setCasilla(R.id.g3);
        board[6][6].setCasilla(R.id.g2);
        board[7][6].setCasilla(R.id.g1);

        board[0][7].setCasilla(R.id.h8);
        board[1][7].setCasilla(R.id.h7);
        board[2][7].setCasilla(R.id.h6);
        board[3][7].setCasilla(R.id.h5);
        board[4][7].setCasilla(R.id.h4);
        board[5][7].setCasilla(R.id.h3);
        board[6][7].setCasilla(R.id.h2);
        board[7][7].setCasilla(R.id.h1);

        /*
        whitePieces = new ArrayList<Piece>();
        blackPieces = new ArrayList<Piece>();

        Piece whiteKing = new King(PieceType.KING, 1, R.drawable.whiteking);
        Piece whiteBishop1 = new Bishop(PieceType.BISHOP, 1, R.drawable.whitebishop);
        Piece whiteBishop2 = new Bishop(PieceType.BISHOP, 1, R.drawable.whitebishop);
        Piece whiteQueen = new Queen(PieceType.QUEEN, 1, R.drawable.whitequeen);
        Piece whiteRook1 = new Rook(PieceType.ROOK, 1, R.drawable.whiterook);
        Piece whiteRook2 = new Rook(PieceType.ROOK, 1, R.drawable.whiterook);
        Piece whiteKnight1 = new Knight(PieceType.KNIGHT, 1, R.drawable.whiteknight);
        Piece whiteKnight2 = new Knight(PieceType.KNIGHT, 1, R.drawable.whiteknight);
        Piece whitePawn1 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn2 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn3 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn4 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn5 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn6 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn7 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);
        Piece whitePawn8 = new Pawn(PieceType.PAWN, 1, R.drawable.whitepawn);


        Piece blackKing = new King(PieceType.KING, 0, R.drawable.blackking);
        Piece blackQueen = new Queen(PieceType.QUEEN, 0, R.drawable.blackqueen);
        Piece blackRock1 = new Rook(PieceType.ROOK, 0, R.drawable.blackrook);
        Piece blackRock2 = new Rook(PieceType.ROOK, 0, R.drawable.blackrook);
        Piece blackBishop1 = new Bishop(PieceType.BISHOP, 0, R.drawable.blackbishop);
        Piece blackBishop2 = new Bishop(PieceType.BISHOP, 0, R.drawable.blackbishop);
        Piece blackKnight1 = new Knight(PieceType.KNIGHT, 0, R.drawable.blackknight);
        Piece blackKnight2 = new Knight(PieceType.KNIGHT, 0, R.drawable.blackknight);
        Piece blackPawn1 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn2 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn3 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn4 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn5 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn6 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn7 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);
        Piece blackPawn8 = new Pawn(PieceType.PAWN, 0, R.drawable.blackpawn);


        whitePieces.add(whitePawn1);
        whitePieces.add(whitePawn2);
        whitePieces.add(whitePawn3);
        whitePieces.add(whitePawn4);
        whitePieces.add(whitePawn5);
        whitePieces.add(whitePawn6);
        whitePieces.add(whitePawn7);
        whitePieces.add(whitePawn8);
        whitePieces.add(whiteRook1);
        whitePieces.add(whiteKnight1);
        whitePieces.add(whiteBishop1);
        whitePieces.add(whiteKing);
        whitePieces.add(whiteQueen);
        whitePieces.add(whiteBishop2);
        whitePieces.add(whiteKnight2);
        whitePieces.add(whiteRook2);

        blackPieces.add(blackRock1);
        blackPieces.add(blackKnight1);
        blackPieces.add(blackBishop1);
        blackPieces.add(blackKing);
        blackPieces.add(blackQueen);
        blackPieces.add(blackBishop2);
        blackPieces.add(blackKnight2);
        blackPieces.add(blackRock2);
        blackPieces.add(blackPawn1);
        blackPieces.add(blackPawn2);
        blackPieces.add(blackPawn3);
        blackPieces.add(blackPawn4);
        blackPieces.add(blackPawn5);
        blackPieces.add(blackPawn6);
        blackPieces.add(blackPawn7);
        blackPieces.add(blackPawn8);

        int cont = 0;
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 8; j++) {
                board[i][j].setPiece(blackPieces.get(cont));
                blackPieces.get(cont).setPosition(board[i][j]);
                cont++;
                Log.e("pieza", board[i][j].actualPiece.toString());
            }
        }

        cont = 0;
        for(int i = 6; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                board[i][j].setPiece(whitePieces.get(cont));
                whitePieces.get(cont).setPosition(board[i][j]);
                cont++;
                Log.e("pieza", board[i][j].actualPiece.toString());
            }
        }
*/
     //   cmd = new CheckmateDetector(this, whitePieces, blackPieces, (King)whiteKing, (King)blackKing);
        initializePieces();
    }

    private void initializePieces() {

        for (int x = 0; x < 8; x++) {
            board[1][x].put(new Pawn(0, board[1][x], R.drawable.blackpawn));
            board[6][x].put(new Pawn(1, board[6][x], R.drawable.whitepawn));
        }

        board[7][3].put(new Queen(1, board[7][3], R.drawable.whitequeen));
        board[0][3].put(new Queen(0, board[0][3], R.drawable.blackqueen));

        King bk = new King(0, board[0][4], R.drawable.blackking);
        King wk = new King(1, board[7][4], R.drawable.whiteking);
        board[0][4].put(bk);
        board[7][4].put(wk);

        board[0][0].put(new Rook(0, board[0][0], R.drawable.blackrook));
        board[0][7].put(new Rook(0, board[0][7], R.drawable.blackrook));
        board[7][0].put(new Rook(1, board[7][0], R.drawable.whiterook));
        board[7][7].put(new Rook(1, board[7][7], R.drawable.whiterook));

        board[0][1].put(new Knight(0, board[0][1], R.drawable.blackknight));
        board[0][6].put(new Knight(0, board[0][6], R.drawable.blackknight));
        board[7][1].put(new Knight(1, board[7][1], R.drawable.whiteknight));
        board[7][6].put(new Knight(1, board[7][6], R.drawable.whiteknight));

        board[0][2].put(new Bishop(0, board[0][2], R.drawable.blackbishop));
        board[0][5].put(new Bishop(0, board[0][5], R.drawable.blackbishop));
        board[7][2].put(new Bishop(1, board[7][2], R.drawable.whitebishop));
        board[7][5].put(new Bishop(1, board[7][5], R.drawable.whitebishop));

        Bpieces = new LinkedList<Piece>();
        Wpieces = new LinkedList<Piece>();

        for(int y = 0; y < 2; y++) {
            for (int x = 0; x < 8; x++) {
                Bpieces.add(board[y][x].getOccupyingPiece());
                Wpieces.add(board[7-y][x].getOccupyingPiece());
            }
        }
        repaint();

        cmd = new CheckmateDetector(this, Wpieces, Bpieces, wk, bk);
    }

    public LinkedList<Piece> Bpieces;
    public LinkedList<Piece> Wpieces;

    public boolean isMoving = false;
    public Casilla selectedCasilla = null;

    private int currX;
    private int currY;
    private Piece currPiece;
    private boolean whiteTurn;
    public List<Casilla> movable;

    private CheckmateDetector cmd;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        MediaPlayer mp = MediaPlayer.create(InGameActivity.this, R.raw.wood_block);
        mp.start();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(v.getId()==board[i][j].button.getId()){
                    /*if(!isMoving){
                        if(board[i][j].actualPiece != null) {
                            isMoving = true;
                            selectedCasilla = board[i][j];
                        }
                        Log.e("booleanismoving", String.valueOf(isMoving) + selectedCasilla.pos.x + " "+selectedCasilla.pos.y);
                    }
                    else {
                        Log.e("booleanismoving", String.valueOf(selectedCasilla.actualPiece.canMove(selectedCasilla, board[i][j])));
                        if(selectedCasilla.actualPiece.canMove(selectedCasilla, board[i][j])){
                            Log.e("booleanismoving", "movida");
                            board[i][j].setPiece(selectedCasilla.removePiece());
                            Log.e("booleanismoving", "movida");
                            isMoving = false;
                        }
                    }*/


                   // board[i][j].
                    System.out.println(board[i][j].button.getId());
                   // board[i][j].button.setBackgroundColor(Color.green(200));

                    if(!isMoving){
                        currX = i;
                        currY = j;

                        if(board[i][j].isOccupied()){
                            currPiece = board[i][j].getOccupyingPiece();
                            if (currPiece.getColor() == 0 && whiteTurn)
                                return;
                            if (currPiece.getColor() == 1 && !whiteTurn)
                                return;
                            board[i][j].setDisplay(false);
                            isMoving = true;
                        }
                        repaint();
                    }
                    else{
                        Casilla sq = (Casilla) board[i][j];

                        if (currPiece != null) {
                            if (currPiece.getColor() == 0 && whiteTurn)
                                return;
                            if (currPiece.getColor() == 1 && !whiteTurn)
                                return;

                            List<Casilla> legalMoves = currPiece.getLegalMoves(this);
                            movable = cmd.getAllowableSquares(whiteTurn);

                            if (legalMoves.contains(sq) && movable.contains(sq) && cmd.testMove(currPiece, sq)) {
                                sq.setDisplay(true);
                                currPiece.move(sq);
                                cmd.update();
                                //sound
                                mp.start();
                                if (cmd.blackCheckMated()) {
                                    currPiece = null;
                                    repaint();
                                    Log.e("pieza", "cgheckmate");
                                } else if (cmd.whiteCheckMated()) {
                                    currPiece = null;
                                    repaint();
                                    Log.e("pieza", "cgheckmate");
                                } else {
                                    currPiece = null;
                                    whiteTurn = !whiteTurn;
                                    movable = cmd.getAllowableSquares(whiteTurn);
                                }

                            } else {
                                currPiece.getPosition().setDisplay(true);
                                currPiece = null;
                            }
                        }
                        repaint();
                        isMoving = false;
                    }


                }
            }
        }
    }

    public void repaint() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Casilla sq = board[y][x];
                sq.paintComponent();
            }
        }

      /*  if (currPiece != null) {
            if ((currPiece.getColor() == 1 && whiteTurn)
                    || (currPiece.getColor() == 0 && !whiteTurn)) {
                int i = currPiece.sprite;
            }
        }*/
    }



}
